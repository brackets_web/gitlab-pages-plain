const express = require("express");
const bodyParser = require('body-parser');
const app = express();
const port = 3333;
// const find = require('local-devices');

app.use(express.static("public"));
// app.use(bodyParser.json());
app.use(express.urlencoded());

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

app.get('/test2', (req, res) => {
  res.send('hello world')
});

app.get('/test', (req, res) => {
  // const data = { uuid7829437: 'Hi', to: 'Mom' }
  // res.send(data)
  const htmlFormResponse = `
  <head>
    <link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
  </head>
  <body style="background-color:#212121; padding:0px; margin:0px;">
    <p style="margin: 0;">getting data</p>
  </body> 
  `;
  res.send(htmlFormResponse);
});
app.post('/test', (req, res) => {
  console.log(req.body)
  // res.sendStatus(200).json({ message: 'was created' })
  const htmlFormResponse = `
  <head>
    <link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
  </head>
  <body style="background-color:#212121; padding:0px; margin:0px;">
    <p style="margin: 0;">room was created</p>
  </body> 
  `
  res.send(htmlFormResponse)
});

app.post('/login', (req, res) => {
  Math.random() > 0.5
    ? res.sendStatus(401)
    : res.json({ message: "login successfull" });
});
