// VIEWS
import "./js/components/main.component.js";
import "./js/components/rooms.component.js";
import "./js/components/header.component.js";
import "./js/components/footer.component.js";
import "./js/components/room-list.component.js";
import "./js/components/room-create.component.js";
import "./js/services/crud.service.js";
import "./js/components/room-detail.component.js";
import { getUserTimeZone } from './js/helper.js';

// SERVICES
import "./js/services/store.service.js";
import "./js/services/room.service.js";

// HELPER
import "./js/helper.js";

const response = await fetch("./i18n/en.json");
const data = await response.json();

const setWebsitesDefaults = async () => {
  document.title = data.title;
  const userTimeZone = getUserTimeZone();
  if (userTimeZone) {
    // console.log("User Timezone:", userTimeZone);
  }
};
setWebsitesDefaults();
