const create = async (url) => {
  let response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    // body: JSON.stringify(data),
  });
  let data = await response.json();
  return data;
};
const getAll = async (url) => {
  let response = await fetch(url, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
    // body: JSON.stringify(data),
  });
  let data = await response.json();
  return data;
};
const getOne = async (url, id) => {
  let response = await fetch(url, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
  let data = await response.json();
  return data;
};
const update = async (url, id) => {
  let response = await fetch(url, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
  let data = await response.json();
  return data;
};
const remove = async (url, id) => {
  let response = await fetch(url, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
  let data = await response.json();
  return data;
};

export { create, getAll, getOne, update, remove };