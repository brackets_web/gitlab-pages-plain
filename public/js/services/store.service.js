import { Room } from './../interfaces/scrum.interfaces.js';

const rooms = new PouchDB('rooms');
const cards = new PouchDB('cards');
const users = new PouchDB('users');

const remoteCouch = false;

rooms.changes({
  since: 'now',
  live: true
}).on('change', GetRooms);

// CRUD

function CreateRoom(room) {
  // TODO: 'test room' => form.name
  const newRoom = new Room(new Date().toISOString(), room.name);
  return rooms
    .put(newRoom)
    .then((doc) => {
      return doc;
    })
    .catch((err) => {
      console.error(err);
      return null;
    });
}

function GetRooms() {
  return rooms.allDocs({ include_docs: true, descending: true })
    .then((docs) => {
      return docs;
    }).catch((err) => {
      console.error(err);
      return null;
    });
}

function GetRoom(id) {
  return rooms.get(id)
    .then((doc) => {
      return doc;
    }).catch((err) => {
      console.error(err);
      return null;
    });
}

function UpdateRoom(id, room) {
  rooms.get(id)
    .then((doc) => {
      return db.put({
        // _id: 'mydoc',
        _rev: doc._rev,
       name: room.name
      });
    }).then((updatedDoc) => {
      return updatedDoc;
    }).catch((err) => {
      console.error(err);
      return null;
    });
}

function DeleteRoom(id) {
  db.remove(id);
}

// db.changes().on('change', function () {
//   console.log('Ch-Ch-Changes');
// });
// db.replicate.to('http://example.com/mydb');

export { CreateRoom, GetRooms, GetRoom, UpdateRoom, DeleteRoom };