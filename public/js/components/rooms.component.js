import { GetRooms } from "./../services/room.service.js";
class Rooms extends HTMLElement {
  constructor() {
    super();
    // this._internals = this.attachInternals();
    this.rooms = GetRooms()
      .then((data) => {
        if (data.rows.length >= 1) {
          const appList = document.createElement("app-room-list");
          appList.setAttribute("data", `${JSON.stringify(data.rows)}`);
          const templateContent = document.getElementById("rooms");
          templateContent.appendChild(appList);
        }
        return data.rows;
      })
      .catch((err) => {
        console.error(err);
        return null;
      });
  }

  // static get observedAttributes() {
  //   return ["rooms"];
  // }

  connectedCallback() {
    this.innerHTML = `
      <app-room-create></app-room-create>
      <section id="rooms">
        <p>Rooms:</p>
      </section>
    `;
  }

  disconnectedCallback() {}
  adoptedCallback() {}
  attributeChangedCallback(name, oldValue, newValue) {
    // TODO: reload if new was added
    // if (name === "rooms" && oldValue !== newValue) {
    //   this.rooms = newValue;
    // }
  }
}

customElements.define("app-rooms", Rooms);
