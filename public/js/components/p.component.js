customElements.define(
  "app-p",
  class extends HTMLElement {
    constructor() {
      super();
      fetch("p.html").then((response) => {
        this.innerHTML = response.text();
        return;
      });
      let template = document.getElementById("app-paragraph");
      let templateContent = template.content;

      const shadowRoot = this.attachShadow({ mode: "open" });
      shadowRoot.appendChild(templateContent.cloneNode(true));
    }
    connectedCallback() {
      
    }
  }
);
