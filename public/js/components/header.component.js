class Header extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = `
      <header>
        <nav aria-label="Second navbar example">
        <ul>
          <li><a href="#">rooms create / join</a></li>
          <li><a href="">about</a></li>
        </ul>
        </nav>
      </header>
    `;
  }
  disconnectedCallback() {}
  adoptedCallback() {}
  attributeChangedCallback(name, oldValue, newValue) {}
}

customElements.define("app-header", Header);
