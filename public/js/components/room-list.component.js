class AppRoomList extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this._data = [];
  }

  static get observedAttributes() {
    return ["data"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === "data" && oldValue !== newValue) {
      this._data = JSON.parse(newValue);
      this.displayData();
    }
  }

  connectedCallback() {
    this.render();
    this.displayData();
  }

  render() {
    const template = document.createElement("template");
    template.innerHTML = `
      <ul id="dataList"></ul>
      <div id="lala"></div>
    `;
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }

  displayData() {
    const dataList = this.shadowRoot.getElementById("dataList");
    if (dataList) {
      // dataList.innerHTML = "";
      this._data.forEach((item) => {
        const li = document.createElement("li");
        li.setAttribute("data", `${JSON.stringify(item.doc)}`);
        li.textContent = `name: ${item.doc.name} passwor: ${ item.doc.password }`;
        dataList.appendChild(li);
        li.addEventListener('click', (e) => {
          const docId = JSON.parse(e.target.attributes[0].value)._id;
          const RoomDetailElement = document.createElement("app-room-detail");
          RoomDetailElement.setAttribute("data", `${JSON.stringify(docId)}`);
          RoomDetailElement.setAttribute("id", "roomDetail");

          const lala = this.shadowRoot.getElementById("lala");
          if(lala.hasChildNodes()) {
            const xyz = lala.childNodes[0];
            xyz.remove();
          }
          // lala.removeAttribute('data')
          lala.appendChild(RoomDetailElement);
        })
      });
    }
  }
}

customElements.define("app-room-list", AppRoomList);
