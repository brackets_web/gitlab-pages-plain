import { CreateRoom } from "./../services/room.service.js";
class RoomCreate extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = `
      <form id="form">
        <label for="name">room name:</label><br>
        <input type="text" name="name" autofocus><br>
        <label for="name">room password:</label><br>
        <input type="password" name="pwd" autofocus><br>
        <button type="submit" part="box">send</button>
      </form>
      `;
    // const host = document.querySelector("#host");
    // const shadow = host.attachShadow({ mode: "open" });

    const form = document.getElementById("form");
    form.addEventListener("submit", (event) => {
      event.preventDefault();
      const formData = new FormData(event.target);
      if (formData.get("name") && formData.get("pwd")) {
        CreateRoom({
          name: formData.get("name"),
          pwd: formData.get("pwd"),
        });
        form.reset();
        alert("Room Created!");
        return;
      }
      form.reset();
      alert("Missing Data!");
    });
  }
  disconnectedCallback() {}
  adoptedCallback() {}
  attributeChangedCallback(name, oldValue, newValue) {}
}

customElements.define("app-room-create", RoomCreate);
