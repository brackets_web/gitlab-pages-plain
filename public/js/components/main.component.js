class Main extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
      this.innerHTML = `
        <app-rooms></app-rooms>
      `;
    }
    disconnectedCallback() { }
    adoptedCallback() { }
    attributeChangedCallback(name, oldValue, newValue) {}
}

customElements.define("app-main", Main);