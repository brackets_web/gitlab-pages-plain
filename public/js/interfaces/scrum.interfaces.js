function User(id, name, room) {
  this._id = id;
  this.name = name;
  this.room = room;
}

function Card(id, name, value) {
  this._id = id;
  this.name = name;
  this.value = value;
} 

function Room(id, name, password) {
  this._id = id;
  this.name = name;
  this.password = password;
  // this.completed = completed;
  // this.users = users;
  // this.cards = cards;
}

export { User, Card, Room };
