const $ = (selector) => {
  return document.querySelector(selector);
};

function getUserTimeZone() {
  try {
    const userTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    return userTimeZone;
  } catch (error) {
    console.error("Error getting user timezone:", error);
    return null;
  }
}

export { $, getUserTimeZone };
