import { GetRooms } from "./js/services/room.service.js";

// TODO: working on templates: as Route; as data container

function elementFromHtml(html) {
  const template = document.createElement("template");
  template.innerHTML = html.trim();
  return template.content.firstElementChild;
}
// const rooms = await GetRooms();
// const roomList = elementFromHtml(`app-list`);
// roomList.setAttribute("data", `${JSON.stringify(rooms.rows)}`);
// const templateContent = document.getElementById("rooms");
// templateContent.appendChild(roomList);

/**
<template id="my-paragraph">
      <h1>
        <slot name="my-title">My default title</slot>
      </h1>
      <p>
        <slot name="my-text">My default text</slot>
      </p>
    </template>
    <my-paragraph>
      <span slot="my-text">Let's have some different text!</span>
      <span slot="my-title">Let's have some different Title!</span>
    </my-paragraph>
    <script>
      customElements.define(
        "my-paragraph",
        class extends HTMLElement {
          constructor() {
            super();
            let template = document.getElementById("my-paragraph");
            let templateContent = template.content;

            const shadowRoot = this.attachShadow({ mode: "open" });
            shadowRoot.appendChild(templateContent.cloneNode(true));
          }
        },
      );
    </script>
    */